/*
 * Validator.cpp
 *
 *  Created on: 7 Mar 2016
 *      Author: luigisantivetti
 */

#include "Validator.h"
#include <regex>
#include <stdlib.h>

#define WARNING_(type, msg) void w_##type() {std::cout << msg << std::endl << "\t> " ;}

WARNING_(INT_P, "input, any or any combination of: +0123456789\nnote: + is optional")
WARNING_(INT_M, "input, any or any combination of: -0123456789")
WARNING_(INT_B, "input, any or any combination of: +-0123456789\nnote: +- are optional, but not together")
WARNING_(FLOAT_P, "input any of: .+0123456789\nnote: + is optional")
WARNING_(FLOAT_M, "input any of: .-0123456789")
WARNING_(FLOAT_B, "input any of: .+-0123456789\nnote: -+ are optional, but not together")
WARNING_(NOUN, "input any of: .+-0123456789\nnote: -+ are optional, but not together")
WARNING_(ALPHA, "input any of: A..Z a..z 0..9")
WARNING_(ALPHA_W, "input any of: A..Z a..z 0..9 any white space allowed")
WARNING_(ADDRESS, "input any of: Address MUST be an alphanumeric not empty string")
WARNING_(POSTCODE, "input any of: Postcode MUST be from 3 to 6 alphanumeric digits long")
WARNING_(EMAIL, "input any of: alphanumeric@alphanumeric.alphanumeric \n(Domain max length from 2 to 4 digits)")
WARNING_(PHONE, "input any of: Phone number MUST be only numbers, from 5 to 10 digits long")
WARNING_(INPUT, "Type your input: ")

using namespace std;

Validator::Validator() {
}

Validator::~Validator() {
}

void Validator::mode(Mode m, void (*f)() ){
    if(m==VERBOSE)
        f();
}

void Validator::mode(Mode m, void (*f)(), const std::string& msg ){
    if(m==VERBOSE)
		cout << msg;
	Validator::mode(m, f);
}

const bool Validator::isValidFloat(std::string& allocator, Sign sign, Mode m) {

	switch (sign)
	{
		case PLUS:
		{
			regex regular_exp("((\\-)?[[:digit:]]+)((\\.|,)(([[:digit:]]+)?))?");

			if(!regex_match(allocator, regular_exp))
				{
                    mode(m, w_FLOAT_P);
					return false;
				}
				else
					return true;

			break;
		}
		case MINUS:
		{
			regex regular_exp("((\\-)[[:digit:]]+)((\\.|,)(([[:digit:]]+)?))?");

			if(!regex_match(allocator, regular_exp))
				{
                    mode(m, w_FLOAT_M);
                    return false;
				}
				else
					return true;

			break;
		}
		case BOTH:
		{
			regex regular_exp("((\\+|-)?[[:digit:]]+)((\\.|,)(([[:digit:]]+)?))?");

			if(!regex_match(allocator, regular_exp))
				{
					mode(m, w_FLOAT_B);
                    return false;
				}
				else
					return true;

			break;
		}
		default:{return false; break;}
	}
}

const bool Validator::isValidInt(std::string& allocator, Sign sign, Mode m) {

	switch (sign)
		{
			case PLUS:
			{
				regex regular_exp("((\\+)?[[:digit:]]+)");

				if(!regex_match(allocator, regular_exp))
					{
					
                        mode(m, w_INT_P);
                        return false;
					}
					else
						return true;

				break;
			}
			case MINUS:
			{
				regex regular_exp("((\\-)[[:digit:]]+)");

				if(!regex_match(allocator, regular_exp))
					{
                        mode(m, w_INT_M);
                        return false;
					}
					else
						return true;

				break;
			}
			case BOTH:
			{
				regex regular_exp("((\\+|-)?[[:digit:]]+)");

				if(!regex_match(allocator, regular_exp))
					{
						mode(m, w_INT_B);
                        return false;
					}
					else
						return true;

				break;
			}
			default:{return false; break;}
		}
}

const bool Validator::isValidNoun(std::string& allocator, Mode m) {

	regex regular_exp("([a-zA-Z]+)");

	if(!regex_match(allocator, regular_exp))
		{
            mode(m, w_NOUN);
			return false;
		}
		else
			return true;
}

const bool Validator::isValidAlpha(std::string& allocator, Mode m) {
    
    regex regular_exp("[^\\n]?([a-zA-Z0-9]+)([^ ]+)?");
    
    if(!regex_match(allocator, regular_exp))
    {
        mode(m, w_ALPHA);
        return false;
    }
    else
        return true;
}

const bool Validator::isValidAlphaWhite(std::string& allocator, Mode m) {

    regex regular_exp("([^\\n][a-zA-Z0-9([[:space:]]*)]+)");

    if(!regex_match(allocator, regular_exp))
    {
        mode(m, w_ALPHA_W);
        return false;
    }
    else
        return true;
}

const bool Validator::isValidAddress(std::string& allocator, Mode m) {
	regex regular_exp("(([0-9]*[a-zA-Z]*)?[a-zA-Z0-9]+)");

	if(!regex_match(allocator, regular_exp))
		{
            mode(m, w_ADDRESS);
			return false;
		}
		else
			return true;
}

const bool Validator::isValidPostcode(std::string& allocator, Mode m) {
	regex regular_exp("([a-zA-Z0-9]{3,6})");

		if(!regex_match(allocator, regular_exp))
			{
                mode(m, w_POSTCODE);
				return false;
			}
			else
				return true;
}

const bool Validator::isValidEmail(std::string& allocator, Mode m) {
	regex regular_exp("([A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4})");

	if(!regex_match(allocator, regular_exp))
		{
            mode(m, w_EMAIL);
			return false;
		}
		else
			return true;
}

const bool Validator::isValidPhone(std::string& allocator, Mode m) {
	regex regular_exp("([0-9]{5,10})");

		if(!regex_match(allocator, regular_exp))
			{
                mode(m, w_PHONE);
				return false;
			}
			else
				return true;

}

const int Validator::io_Int(Sign s, Mode m){
    
    string _s;
    do{
    	mode(m, w_INPUT, "Integer | ");
        std::cin >> _s;
    } while ( !isValidInt(_s, s, SILENT) );
    return ( atoi(_s.c_str()) );

}

const short Validator::io_Short(Sign s, Mode m){

    string _s;
    do{
    	mode(m, w_INPUT, "Short | ");
        std::cin >> _s;
    } while ( !isValidInt(_s, s, SILENT) );
    return ( atoi(_s.c_str()) );
}

const float Validator::io_Float(Sign s, Mode m){

    string _input;
	do{
		mode(m, w_INPUT, "Float | ");
        std::cin >> _input;
    } while ( !isValidFloat(_input, s, SILENT) );
    return ( atof(_input.c_str() ) );
}

const double Validator::io_Double(Sign s, Mode m){

    string _input;
	do{
		mode(m, w_INPUT, "Double | ");
        std::cin >> _input;
    } while ( !isValidFloat(_input, s, SILENT) );
    return ( atof(_input.c_str() ) );
}

const std::string Validator::io_Alpha(Mode m){

	char _input[255];
    string _s;
	do{
		mode(m, w_INPUT, "Alpha | ");
		cin.getline(_input, sizeof(_input));
		_s = string(_input);
		cout << _s << endl;
    } while ( !isValidAlpha(_s, SILENT) );
    return _input;
}

const std::string Validator::io_AlphaWhite(Mode m){

    char _input[255];
    string _s;
	do{
		mode(m, w_INPUT, "Alp_W | ");
		cin.getline(_input, sizeof(_input));
		_s = string(_input);
		cout << _s << endl;
    } while ( !isValidAlphaWhite(_s, SILENT) );
    return _input;
}

const std::string Validator::io_Noun(Mode m){

    string _input;
	do{
		mode(m, w_INPUT, "Aa.Zz | ");
        std::cin >> _input;
    } while ( !isValidNoun(_input, SILENT) );
    return _input;
}

const std::string Validator::io_Email(Mode m){

    string _input;
	do{
		mode(m, w_INPUT, "Email | ");
        std::cin >> _input;
    } while ( !isValidEmail(_input, SILENT) );
    return _input;
}

const std::string Validator::io_Phone(Mode m){

    string _input;
	do{
		mode(m, w_INPUT, "Phone | ");
        std::cin >> _input;
    } while ( !isValidEmail(_input, SILENT) );
    return _input;
}





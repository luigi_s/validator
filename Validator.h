/*
 * Validator.h
 *
 *  Created on: 7 Mar 2016
 *      Author: luigisantivetti
 */
#include <string>
#include <iostream>

#ifndef VALIDATOR_H_
#define VALIDATOR_H_

enum Sign {PLUS = 1, MINUS = -1, BOTH = 0};
enum Mode {VERBOSE, SILENT};

class Validator {
public:
	Validator();
	virtual ~Validator();

	static const bool isValidFloat(std::string&, Sign sign, Mode m);
	static const bool isValidInt(std::string&, Sign sign, Mode m);
	static const bool isValidNoun(std::string&, Mode m);
    static const bool isValidAlpha(std::string&, Mode m);
    static const bool isValidAlphaWhite(std::string&, Mode m);
	static const bool isValidAddress(std::string&, Mode m);
	static const bool isValidPostcode(std::string&, Mode m);
	static const bool isValidEmail(std::string&, Mode m);
	static const bool isValidPhone(std::string&, Mode m);
    
    static const int io_Int(Sign s, Mode m);
    static const float io_Float(Sign s, Mode m);
    static const double io_Double(Sign s, Mode m);
    static const short io_Short(Sign s, Mode m);
    static const std::string io_Alpha(Mode m);
    static const std::string io_AlphaWhite(Mode m);
    static const std::string io_Phone(Mode m);
    static const std::string io_Email(Mode m);
    static const std::string io_Noun(Mode m);

private:
    static void mode(Mode m, void (*f)());
    static void mode(Mode m, void (*f)(), const std::string&);
};

#endif /* VALIDATOR_H_ */

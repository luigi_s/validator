### What is this repository for? ###

* Public repository for Validator, a C++ static library
* Linked statically it provides a fast set of functions for validating user input from the keyboard

### How do I get set up? ###

* git clone https://bitbucket.org/luigi_s/validator.git
* gcc -c validator.cpp -o validator.o
* ar rcs validator.a validator.o
* gcc -static <your_main_file.c> -L. -lvalidator -o statically_linked
